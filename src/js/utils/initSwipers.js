import Swiper from 'swiper';

window.Swiper = Swiper;

Swiper.instances = {};
let swipers = document.getElementsByClassName('swiper-container');
for (let swiper of swipers) {
    let preset = swiper.dataset.preset;
    let options;
    switch (preset) {
        case 'reviews':
            options = {
                slidesPerView: 1,
                loop: true,
                // navigation: {
                //     nextEl: nextButton,
                //     prevEl: prevButton,
                // },
                centeredSlides: true,
                effect: 'coverflow',
                coverflowEffect: {
                    rotate: 0, // Slide rotate in degrees
                    stretch: 0, // Stretch space between slides (in px)
                    depth: 150, // Depth offset in px (slides translate in Z axis)
                    modifier: 1, // Effect multipler
                    slideShadows: false, // Enables slides shadows
                },
                navigation: {
                    prevEl: '.reviews__nav-btn_prev',
                    nextEl: '.reviews__nav-btn_next',
                },
                breakpoints: {
                    721: {
                        slidesPerView: 2,
                    }
                },
                observer: true
            };
            break;
        default:
            let slidesPerView = parseInt(swiper.dataset.slidesperview || 1);
            let spaceBetween = parseInt(swiper.dataset.spacebetween || 0);
            let nextButton = swiper.dataset.next;
            let prevButton = swiper.dataset.prev;
            let paginationEl = swiper.dataset.paginationel;
            let paginationType = swiper.dataset.paginationtype;
            let direction = swiper.dataset.direction || 'horizontal';
            let name = swiper.dataset.name;
            let allowTouchMove = swiper.dataset.allowtouchmove !== 'false';
            let mousewheel = swiper.dataset.mousewheel ? {invert: false, releaseOnEdges: false} : false;
            options = {
                slidesPerView: slidesPerView,
                spaceBetween: spaceBetween,
                allowTouchMove: allowTouchMove,
                direction: direction,
                mousewheel: mousewheel,
                navigation: {
                    nextEl: nextButton,
                    prevEl: prevButton,
                },
                pagination: {
                    el: paginationEl,
                    type: paginationType,
                    clickable: true
                }
            };
            break;
    }

    // console.log(options);

    let sw = new Swiper(swiper, options);

    if (name) {
        Swiper.instances[name] = sw;
    }
}

for (let swiper of swipers) {
    let name = swiper.dataset.name;
    let controlSwiper = swiper.dataset.controlswiper;

    if (controlSwiper) {
        Swiper.instances[name].controller.control = Swiper.instances[controlSwiper];
    }
}