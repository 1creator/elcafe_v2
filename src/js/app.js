import './utils/initSwipers';
import AOS from 'aos';
import bsn from 'bootstrap.native/dist/bootstrap-native-v4';
import 'simplebar/dist/simplebar';

AOS.init();

let map, mapOverlay;

function makeOverlay(icon, offset) {
    // document.body.style.overflowY = 'hidden';
    const rect = document.getElementById('map').getBoundingClientRect();

    mapOverlay = document.createElement('div');
    mapOverlay.className = 'network__overlay';
    document.body.appendChild(mapOverlay);

    let img = document.createElement('img');
    img.className = 'network__overlay-cup';
    img.src = icon;
    img.style.left = rect.left + offset.x + 'px';
    img.style.top = rect.top + offset.y + 'px';

    mapOverlay.appendChild(img);
}

function removeOverlay() {
    document.body.removeChild(mapOverlay);
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: mapCenter,
        zoom: mapZoomLevel,
    });

    const swiper = document.getElementsByClassName('map-popup__swiper')[0].swiper;
    const modalEl = document.getElementById('mapPopup');
    const modal = new bsn.Modal(modalEl);
    window.modal = modal;

    const popupCupEl = modalEl.getElementsByClassName('map-popup__cup')[0];
    const popupCaptionEl = modalEl.getElementsByClassName('map-popup__caption')[0];
    const popupAddressEl = modalEl.getElementsByClassName('map-popup__address')[0];
    const popupWatchImgEl = modalEl.getElementsByClassName('map-popup__watch-link-img')[0];
    const popupWatchLinkEl = modalEl.getElementsByClassName('map-popup__watch-link')[0];
    const popupScheduleEl = modalEl.getElementsByClassName('map-popup__schedule')[0];
    const popupSlidesEl = modalEl.getElementsByClassName('swiper-wrapper')[0];

    modalEl.addEventListener('shown.bs.modal', function (event) {
        swiper.update();
    }, false);

    for (const point of points) {
        const marker = new google.maps.Marker({
            map: map,
            position: point.position,
            icon: point.icon,
        });

        if (point.icon === PointType.SoonOpen) continue;

        google.maps.event.addListener(marker, 'mouseover', function () {
            var scale = Math.pow(2, map.getZoom());
            var nw = new google.maps.LatLng(
                map.getBounds().getNorthEast().lat(),
                map.getBounds().getSouthWest().lng()
            );
            var worldCoordinateNW = map.getProjection().fromLatLngToPoint(nw);
            var worldCoordinate = map.getProjection().fromLatLngToPoint(this.getPosition());
            var pixelOffset = new google.maps.Point(
                Math.floor((worldCoordinate.x - worldCoordinateNW.x) * scale),
                Math.floor((worldCoordinate.y - worldCoordinateNW.y) * scale)
            );

            makeOverlay(point.icon, pixelOffset);
        });

        google.maps.event.addListener(marker, 'mouseout', function () {
            removeOverlay();
        });

        google.maps.event.addListener(marker, 'click', function () {
            popupCupEl.src = point.icon;
            popupCaptionEl.innerHTML = point.caption;
            popupAddressEl.innerHTML = point.address;
            popupWatchImgEl.src = point.watchType;
            popupWatchLinkEl.href = point.watchLink;
            popupScheduleEl.innerHTML = point.schedule;
            popupSlidesEl.innerHTML = '';
            for (const image of point.images) {
                popupSlidesEl.innerHTML += `<div class="swiper-slide">
                                                <img src="${image}" class="map-popup__slide-img">
                                            </div>`;
            }

            modal.show();
        });
    }
}

window.initMap = initMap;

const founderTabs = document.getElementsByClassName('founders__read-btn');
const founderPanes = document.getElementsByClassName('founders__founder');

founderTabs[0].addEventListener('show.bs.tab', function () {
    founderPanes[0].classList.toggle('show-pin', true);
    founderPanes[1].classList.toggle('show-pin', false);
    founderTabs[0].scrollIntoView({block: 'start', behavior: 'smooth'});
});

founderTabs[1].addEventListener('show.bs.tab', function () {
    founderPanes[0].classList.toggle('show-pin', false);
    founderPanes[1].classList.toggle('show-pin', true);
    founderTabs[0].scrollIntoView({block: 'start', behavior: 'smooth'});
});