@import "../../variables";

.franchise-content {
  background: rgba(197, 179, 163, 0.5);
  padding-top: 95px;
  padding-bottom: 136px;

  @include respond-to(sm) {
    padding-top: 50px;
  }

  @include respond-to(lg) {
    padding-bottom: 70px;
  }


  .franchise-content__title {
    text-align: center;
    text-transform: uppercase;
    margin-bottom: 68px;

    @include respond-to(sm) {
      margin-bottom: 43px;
    }
  }

  .franchise-content__item-wrap {
    position: relative;
    width: 50%;
    border-right: 8px solid #D3A950;
    padding-right: 84px;
    transform: translateX(4px);

    @include respond-to(lg) {
      width: 100%;
      border: none !important;
      padding: 0 !important;
      transform: none !important;
      margin-bottom: 60px;
    }

    &:nth-child(even) {
      margin-left: auto;
      border-right: none;
      border-left: 8px solid #D3A950;
      padding-left: 84px;
      padding-right: 0;
      transform: translateX(-4px);

      .franchise-content__item {
        &:before {
          right: auto;
          left: -2%;

          @include respond-to(lg) {
            content: none;
          }
        }
      }

      .franchise-content__item-number {
        right: auto;
        left: -4px;
        margin: auto auto auto -108px;
      }
    }

    &:first-of-type {
      padding-top: 90px;

      &:before {
        content: "";
        position: absolute;
        background: #D3A950;
        width: 24px;
        height: 24px;
        border-radius: 50%;
        top: -4px;
        right: -16px;
        bottom: -4px;

        @include respond-to(lg) {
          content: none;
        }
      }
    }

    &:last-of-type {
      padding-bottom: 90px;
      margin-bottom: 80px;

      &:before {
        content: "";
        position: absolute;
        background: #D3A950;
        width: 24px;
        height: 24px;
        border-radius: 50%;
        right: -16px;
        bottom: -4px;

        @include respond-to(lg) {
          content: none;
        }
      }
    }
  }

  .franchise-content__item {
    margin-bottom: 0;
    margin-top: auto;
    padding: 21px 24px 34px 32px;
    background: #FFFFFF;
    box-shadow: 30px 54px 64px rgba(0, 0, 0, 0.17);
    border-radius: 20px;
    position: relative;
    z-index: 2;

    &:before {
      content: '';
      position: absolute;
      z-index: 1;
      right: -2%;
      top: 0;
      bottom: 0;
      margin: auto;
      width: 23px;
      height: 23px;
      background: rgba(255, 255, 255, 0.6);
      transform: rotate(45deg);

      @include respond-to(lg) {
        content: none;
      }
    }
  }

  .franchise-content__item-title {
    @include respond-to(xl) {
      font-size: 30px;
      font-weight: 500;
    }
  }

  .franchise-content__item-number {
    background: #EA1D19;
    border-radius: 50%;
    position: absolute;
    width: 48px;
    height: 48px;
    padding: 10px;
    top: 0;
    bottom: 0;
    right: -4px;
    margin: auto -108px auto auto;
    text-align: center;
    font-weight: 500;
    font-size: 24px;
    line-height: 34px;
    color: #FFFFFF;

    background-image: url(../images/coffee-cup-icon-brown.svg);
    background-position: center;
    background-repeat: no-repeat;

    @include respond-to(lg) {
      display: none;
    }
  }
}