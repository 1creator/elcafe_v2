const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({
    terser: {
        exclude: /\/app.js/,
    }
});

mix
    .options({processCssUrls: false})
    .js('src/js/app.js', 'dist/js')
    .extract(['swiper', 'aos', 'bootstrap.native', 'simplebar', 'dom7', 'ssr-window'])
    .sass('src/scss/vendor.scss', 'dist/css')
    .sass('src/scss/app.scss', 'dist/css');

mix.copy('src/*.html', 'dist/');
mix.disableSuccessNotifications();