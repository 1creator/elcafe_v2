(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/swiper/js/swiper.esm.bundle.js + 1 modules
var swiper_esm_bundle = __webpack_require__(11);

// CONCATENATED MODULE: ./src/js/utils/initSwipers.js

window.Swiper = swiper_esm_bundle["default"];
swiper_esm_bundle["default"].instances = {};
var swipers = document.getElementsByClassName('swiper-container');
var initSwipers_iteratorNormalCompletion = true;
var initSwipers_didIteratorError = false;
var initSwipers_iteratorError = undefined;

try {
  for (var initSwipers_iterator = swipers[Symbol.iterator](), initSwipers_step; !(initSwipers_iteratorNormalCompletion = (initSwipers_step = initSwipers_iterator.next()).done); initSwipers_iteratorNormalCompletion = true) {
    var initSwipers_swiper = initSwipers_step.value;
    var preset = initSwipers_swiper.dataset.preset;
    var options = void 0;

    switch (preset) {
      case 'reviews':
        options = {
          slidesPerView: 1,
          loop: true,
          // navigation: {
          //     nextEl: nextButton,
          //     prevEl: prevButton,
          // },
          centeredSlides: true,
          effect: 'coverflow',
          coverflowEffect: {
            rotate: 0,
            // Slide rotate in degrees
            stretch: 0,
            // Stretch space between slides (in px)
            depth: 150,
            // Depth offset in px (slides translate in Z axis)
            modifier: 1,
            // Effect multipler
            slideShadows: false // Enables slides shadows

          },
          navigation: {
            prevEl: '.reviews__nav-btn_prev',
            nextEl: '.reviews__nav-btn_next'
          },
          breakpoints: {
            721: {
              slidesPerView: 2
            }
          },
          observer: true
        };
        break;

      default:
        var slidesPerView = parseInt(initSwipers_swiper.dataset.slidesperview || 1);
        var spaceBetween = parseInt(initSwipers_swiper.dataset.spacebetween || 0);
        var nextButton = initSwipers_swiper.dataset.next;
        var prevButton = initSwipers_swiper.dataset.prev;
        var paginationEl = initSwipers_swiper.dataset.paginationel;
        var paginationType = initSwipers_swiper.dataset.paginationtype;
        var direction = initSwipers_swiper.dataset.direction || 'horizontal';
        var _name = initSwipers_swiper.dataset.name;
        var allowTouchMove = initSwipers_swiper.dataset.allowtouchmove !== 'false';
        var mousewheel = initSwipers_swiper.dataset.mousewheel ? {
          invert: false,
          releaseOnEdges: false
        } : false;
        options = {
          slidesPerView: slidesPerView,
          spaceBetween: spaceBetween,
          allowTouchMove: allowTouchMove,
          direction: direction,
          mousewheel: mousewheel,
          navigation: {
            nextEl: nextButton,
            prevEl: prevButton
          },
          pagination: {
            el: paginationEl,
            type: paginationType,
            clickable: true
          }
        };
        break;
    } // console.log(options);


    var sw = new swiper_esm_bundle["default"](initSwipers_swiper, options);

    if (name) {
      swiper_esm_bundle["default"].instances[name] = sw;
    }
  }
} catch (err) {
  initSwipers_didIteratorError = true;
  initSwipers_iteratorError = err;
} finally {
  try {
    if (!initSwipers_iteratorNormalCompletion && initSwipers_iterator["return"] != null) {
      initSwipers_iterator["return"]();
    }
  } finally {
    if (initSwipers_didIteratorError) {
      throw initSwipers_iteratorError;
    }
  }
}

var _iteratorNormalCompletion2 = true;
var _didIteratorError2 = false;
var _iteratorError2 = undefined;

try {
  for (var _iterator2 = swipers[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
    var _swiper = _step2.value;
    var _name2 = _swiper.dataset.name;
    var controlSwiper = _swiper.dataset.controlswiper;

    if (controlSwiper) {
      swiper_esm_bundle["default"].instances[_name2].controller.control = swiper_esm_bundle["default"].instances[controlSwiper];
    }
  }
} catch (err) {
  _didIteratorError2 = true;
  _iteratorError2 = err;
} finally {
  try {
    if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
      _iterator2["return"]();
    }
  } finally {
    if (_didIteratorError2) {
      throw _iteratorError2;
    }
  }
}
// EXTERNAL MODULE: ./node_modules/aos/dist/aos.js
var aos = __webpack_require__(38);
var aos_default = /*#__PURE__*/__webpack_require__.n(aos);

// EXTERNAL MODULE: ./node_modules/bootstrap.native/dist/bootstrap-native-v4.js
var bootstrap_native_v4 = __webpack_require__(76);
var bootstrap_native_v4_default = /*#__PURE__*/__webpack_require__.n(bootstrap_native_v4);

// EXTERNAL MODULE: ./node_modules/simplebar/dist/simplebar.js
var simplebar = __webpack_require__(80);

// CONCATENATED MODULE: ./src/js/app.js




aos_default.a.init();
var map, mapOverlay;

function makeOverlay(icon, offset) {
  // document.body.style.overflowY = 'hidden';
  var rect = document.getElementById('map').getBoundingClientRect();
  mapOverlay = document.createElement('div');
  mapOverlay.className = 'network__overlay';
  document.body.appendChild(mapOverlay);
  var img = document.createElement('img');
  img.className = 'network__overlay-cup';
  img.src = icon;
  img.style.left = rect.left + offset.x + 'px';
  img.style.top = rect.top + offset.y + 'px';
  mapOverlay.appendChild(img);
}

function removeOverlay() {
  document.body.removeChild(mapOverlay);
}

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: mapCenter,
    zoom: mapZoomLevel
  });
  var swiper = document.getElementsByClassName('map-popup__swiper')[0].swiper;
  var modalEl = document.getElementById('mapPopup');
  var modal = new bootstrap_native_v4_default.a.Modal(modalEl);
  window.modal = modal;
  var popupCupEl = modalEl.getElementsByClassName('map-popup__cup')[0];
  var popupCaptionEl = modalEl.getElementsByClassName('map-popup__caption')[0];
  var popupAddressEl = modalEl.getElementsByClassName('map-popup__address')[0];
  var popupWatchImgEl = modalEl.getElementsByClassName('map-popup__watch-link-img')[0];
  var popupWatchLinkEl = modalEl.getElementsByClassName('map-popup__watch-link')[0];
  var popupScheduleEl = modalEl.getElementsByClassName('map-popup__schedule')[0];
  var popupSlidesEl = modalEl.getElementsByClassName('swiper-wrapper')[0];
  modalEl.addEventListener('shown.bs.modal', function (event) {
    swiper.update();
  }, false);
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    var _loop = function _loop() {
      var point = _step.value;
      var marker = new google.maps.Marker({
        map: map,
        position: point.position,
        icon: point.icon
      });
      if (point.icon === PointType.SoonOpen) return "continue";
      google.maps.event.addListener(marker, 'mouseover', function () {
        var scale = Math.pow(2, map.getZoom());
        var nw = new google.maps.LatLng(map.getBounds().getNorthEast().lat(), map.getBounds().getSouthWest().lng());
        var worldCoordinateNW = map.getProjection().fromLatLngToPoint(nw);
        var worldCoordinate = map.getProjection().fromLatLngToPoint(this.getPosition());
        var pixelOffset = new google.maps.Point(Math.floor((worldCoordinate.x - worldCoordinateNW.x) * scale), Math.floor((worldCoordinate.y - worldCoordinateNW.y) * scale));
        makeOverlay(point.icon, pixelOffset);
      });
      google.maps.event.addListener(marker, 'mouseout', function () {
        removeOverlay();
      });
      google.maps.event.addListener(marker, 'click', function () {
        popupCupEl.src = point.icon;
        popupCaptionEl.innerHTML = point.caption;
        popupAddressEl.innerHTML = point.address;
        popupWatchImgEl.src = point.watchType;
        popupWatchLinkEl.href = point.watchLink;
        popupScheduleEl.innerHTML = point.schedule;
        popupSlidesEl.innerHTML = '';
        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
          for (var _iterator2 = point.images[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
            var image = _step2.value;
            popupSlidesEl.innerHTML += "<div class=\"swiper-slide\">\n                                                <img src=\"".concat(image, "\" class=\"map-popup__slide-img\">\n                                            </div>");
          }
        } catch (err) {
          _didIteratorError2 = true;
          _iteratorError2 = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
              _iterator2["return"]();
            }
          } finally {
            if (_didIteratorError2) {
              throw _iteratorError2;
            }
          }
        }

        modal.show();
      });
    };

    for (var _iterator = points[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var _ret = _loop();

      if (_ret === "continue") continue;
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator["return"] != null) {
        _iterator["return"]();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }
}

window.initMap = initMap;
var founderTabs = document.getElementsByClassName('founders__read-btn');
var founderPanes = document.getElementsByClassName('founders__founder');
founderTabs[0].addEventListener('show.bs.tab', function () {
  founderPanes[0].classList.toggle('show-pin', true);
  founderPanes[1].classList.toggle('show-pin', false);
  founderTabs[0].scrollIntoView({
    block: 'start',
    behavior: 'smooth'
  });
});
founderTabs[1].addEventListener('show.bs.tab', function () {
  founderPanes[0].classList.toggle('show-pin', false);
  founderPanes[1].classList.toggle('show-pin', true);
  founderTabs[0].scrollIntoView({
    block: 'start',
    behavior: 'smooth'
  });
});

/***/ }),

/***/ 132:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 139:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 7:
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ 79:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(130);
__webpack_require__(132);
module.exports = __webpack_require__(139);


/***/ })

},[[79,1,2]]]);